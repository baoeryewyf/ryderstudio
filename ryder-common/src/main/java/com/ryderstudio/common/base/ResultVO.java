package com.ryderstudio.common.base;

import com.alibaba.fastjson.JSON;
import com.ryderstudio.common.exception.ErrorEnum;

import java.util.Collection;

public class ResultVO<T> {

    private int code;

    private String msg;

    private T data;

    private Long count;

    public ResultVO(int code, String msg, T data, Long count) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.count = count;
    }

    public ResultVO(){}

    public ResultVO(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResultVO(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static <T> ResultVO<T> fail(int code, String msg) {
        return new ResultVO<T>(code, msg);
    }

    public static <T> ResultVO<T> fail(ErrorEnum ee) {
        return new ResultVO<T>(ee.getCode(), ee.getMessage());
    }

    public static <T> ResultVO<T> fail(ErrorEnum ee, String appendMessage) {
        return new ResultVO<T>(ee.getCode(), ee.getMessage() + "，" + appendMessage);
    }

    public static <T> ResultVO<T> success() {
        //return new ResultVO<T>(HttpStatus.OK.value(), "success");
        return new ResultVO<T>(10000, "success");
    }

    public static <T> ResultVO<T> success(T data) {
        //return new ResultVO<T>(HttpStatus.OK.value(), "success", data);
        Long count = null;
        if (data instanceof Collection){
            count = Long.valueOf(((Collection) data).size());
        }
        return new ResultVO<T>(10000, "success", data, count);
    }
    public static <T> ResultVO<T> success(T data,Long count){
        //ResultVO<T> ok = new ResultVO<>(HttpStatus.OK.value(), "success", data);
        ResultVO<T> ok = new ResultVO<>(10000, "success", data);
        ok.setCount(count);
        return ok;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public String toJsonString(){
        return JSON.toJSONString(this);
    }
}
