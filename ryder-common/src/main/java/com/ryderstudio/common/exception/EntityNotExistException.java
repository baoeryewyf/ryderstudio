package com.ryderstudio.common.exception;

/**
 * @author Ryder
 * @date 2021-06-15
 */

public class EntityNotExistException extends BusinessException {

    public EntityNotExistException(String field, String val) {
        super(ErrorEnum.EntNotExist.getCode(), ErrorEnum.EntNotExist.getMessage()+"，"+field + "："+ val + "，不存在");
    }

}
