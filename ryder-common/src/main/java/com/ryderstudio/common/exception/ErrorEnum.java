package com.ryderstudio.common.exception;

import java.util.stream.Stream;

public enum ErrorEnum {
    // --- 300xx 用户 相关错误 ---
    UserAuthFailed(30000, "用户认证失败"),
    UserDisabled(30001, "用户已禁用"),
    UserPasswordNotMatched(30002, "用户名或密码不正确"),
    UserNameAlreadyExist(30003, "用户名已存在"),
    UserNameNotExist(30004, "用户名不存在"),
    UserPhoneAlreadyExist(30005, "手机号已存在"),
    UserPhoneNotExist(30006, "手机号不存在"),
    UserUnauthorized(30007, "非法用户"),
    UserInviteCodeInvalid(30008, "用户邀请码无效"),
    UserLoginNumberExceed(30009, "用户登录数量超限"),

    UpdatePwFailedForWrongOldPw(30011, "密码修改失败，旧密码错误"),
    UpdatePwFailedForNewSameToOld(30012, "密码修改失败，新密码与旧密码相同"),

    // --- 301xx Session 相关错误 ---
    SessionKeyIsNull(30100, "会话Key不存在"),
    SessionExpired(30101, "会话已过期"),

    // --- 302xx Token令牌 相关错误 ---
    TokenNotPresent(30200,"Token不存在"),
    TokenExpired(30201,"Token已过期"),
    TokenInvalid(30202,"Token无效"),

    // --- 303xx 验证码 相关错误 ---
    VerificationCodeFailed(30300, "验证码校验失败"),
    PhoneNumberIsEmpty(30310, "手机号码为空"),
    CodeMsgSendFailed(30320, "短信发送失败"),

    // --- 400xx 系统访问权限 相关错误 ---
    SystemRequestHeadNotPresent(40000, "缺少system请求头"),
    SystemNotAllowed(40001, "无该系统认证权限，请联系管理员分配权限"),
    VersionNotMatched(40002, "版本不匹配"),

    // --- 401xx 请求参数 相关错误 ---
    MethodArgumentNotValid(40100, "请求参数不正确"),

    // --- 404xx 文件 相关错误 ---
    FileNotExist(40400, "文件不存在"),
    FileIsNotFile(40401, "不是有效的文件"),
    DirectoryNotExist(40410, "目录不存在"),
    DirectoryIsNotDirectory(40411, "不是有效的目录"),

    // --- 504xx 对象操作 相关错误 ---
    EntAlreadyExist(50400, "对象已经存在"), // 对象创建时已经存在
    EntNotExist(50401, "对象不存在"), // 对象查询、修改或删除时不存在

    Unhandled(99999, "未处理的错误"),
    ;

    private Integer code;
    private String message;

    ErrorEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static ErrorEnum of(Integer code) {
        return Stream.of(ErrorEnum.values())
                .filter(e -> e.getCode() == code)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
