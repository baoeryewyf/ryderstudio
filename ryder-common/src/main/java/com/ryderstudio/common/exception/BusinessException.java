package com.ryderstudio.common.exception;

public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private Integer code;

    /**
     * @param code 错误码
     * @param message 错误信息
     */
    public BusinessException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public BusinessException(ErrorEnum errorEnum) {
        super(errorEnum.getMessage());
        this.code = errorEnum.getCode();
    }

    public BusinessException(ErrorEnum errorEnum, String appendMessage) {
        super(errorEnum.getMessage() + "，" + appendMessage);
        this.code = errorEnum.getCode();
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
