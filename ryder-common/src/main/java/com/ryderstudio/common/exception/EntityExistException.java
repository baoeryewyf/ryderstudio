package com.ryderstudio.common.exception;

/**
 * @author Ryder
 * @date 2022-07-06
 */

public class EntityExistException extends BusinessException {

    public EntityExistException(String field, String val) {
        super(ErrorEnum.EntAlreadyExist.getCode(), ErrorEnum.EntAlreadyExist.getMessage()+"，"+field + "："+ val + "，已存在");
    }

}
